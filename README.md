# Style files for rcp

`.clang-format` and `.clang-tidy` for `rcp` sub-groups and projects.

Add this project as a submodule to any project within the `rcp`
group, and create symlinks in the project top level directory to the
files in the submodule.

